package de.cbl.home.jmerge;

import de.cbl.home.jmerge.list.PDFList;
import de.cbl.home.jmerge.util.Constants;
import de.cbl.home.jmerge.util.PDFHelper;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Application extends javafx.application.Application {

    public static final String USER_HOME = "user.home";
    public static final int FIRST_INDEX = 0;
    private Button saveAsPDFBtn;
    private File[] latestSelectedPdfFilesFromFolder;
    private Pane pdfFilesContainer;


    public static void main(String[] args) throws IOException {
        launch(args);
    }

    private String USER_HOME_DIR = System.getProperty(USER_HOME) + File.separator;
    PDFHelper pdfMergeHelper = new PDFHelper();

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("jmerge-pdf");

        Pane container = new VBox(20);
        container.setPadding(new Insets(20, 10, 20, 10));

        Pane body = new VBox();
        pdfFilesContainer = new VBox(10);
        Label selectedPDFsLabel = new Label("Selected PDFs");

        PDFList pdfList = new PDFList();

        pdfFilesContainer.getChildren().addAll(selectedPDFsLabel, pdfList.getListView());

        Pane header = new VBox(20);
        Pane buttonBox = new HBox(20);

        Button selectPDFsFromFolderBtn = new Button("Select PDFs from folder");
        Button addSinglePDFBtn = new Button("Add single PDF");
        Button removeAllPDFsBtn = new Button("Remove all");

        buttonBox.getChildren().addAll(selectPDFsFromFolderBtn, addSinglePDFBtn, removeAllPDFsBtn);

        selectPDFsFromFolderBtn.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File(USER_HOME_DIR));
            directoryChooser.setTitle("Choose a folder with PDFs to import");
            File selectedFolder = directoryChooser.showDialog(stage);

            if(selectedFolder == null) {
                return;
            }

            // filter files for ".pdf"
            latestSelectedPdfFilesFromFolder = selectedFolder.listFiles((file, name) -> name.toLowerCase().endsWith(Constants.PDF_FILE_FORMAT));

            if(selectedFolder.isDirectory() && latestSelectedPdfFilesFromFolder.length > 1) {
                pdfList.addAll(latestSelectedPdfFilesFromFolder);
            }
        });

        addSinglePDFBtn.setOnAction(actionEvent -> {
            FileChooser pdfChooser = new FileChooser();
            pdfChooser.setTitle("Choose PDF file to add it to the list");
            File selectedPDF = pdfChooser.showOpenDialog(stage);

            if(selectedPDF == null) {
                return;
            }

            pdfList.add(selectedPDF);
        });

        removeAllPDFsBtn.setOnAction(actionEvent -> {
            pdfList.clear();
        });

        pdfList.getData().addListener((ListChangeListener<File>) change -> {
            saveAsPDFBtn.setDisable(pdfList.getData().size() < 2);
        });

        header.getChildren().add(buttonBox);
        body.getChildren().addAll(pdfFilesContainer);

        saveAsPDFBtn = new Button("Save as PDF");
        saveAsPDFBtn.setDisable(true);
        saveAsPDFBtn.setOnAction(actionEvent -> {
            FileChooser saveDialog = new FileChooser();
            saveDialog.setInitialDirectory(new File(USER_HOME_DIR));
            FileChooser.ExtensionFilter pdfExtensionFilter = new FileChooser.ExtensionFilter(Constants.PDF_FILE_DESCRIPTION, Constants.PDF_FILE_FORMAT_FILTER);
            saveDialog.getExtensionFilters().add(pdfExtensionFilter);
            File selectedFileToSave = saveDialog.showSaveDialog(stage);

            if(selectedFileToSave == null) {
                return;
            }

            List<File> pdfsToMerge = pdfList.getData().subList(FIRST_INDEX, pdfList.getData().size());

            try {
                pdfMergeHelper.mergeFromFiles(pdfsToMerge, selectedFileToSave.getAbsolutePath());
                pdfMergeHelper.openPDFExternal(selectedFileToSave);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        container.getChildren().addAll(header, body, saveAsPDFBtn);
        StackPane root = new StackPane();
        root.getChildren().add(container);
        stage.setScene(new Scene(root, 700, 400));
        stage.show();
    }

}
