package de.cbl.home.jmerge.list.callbacks;

public interface PDFCellCallback {
    public void onCellRemove(int cellIndex);
}
