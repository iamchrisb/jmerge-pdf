package de.cbl.home.jmerge.list;

import de.cbl.home.jmerge.list.callbacks.PDFCellCallback;
import de.cbl.home.jmerge.util.Constants;
import de.cbl.home.jmerge.util.PDFHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class PDFList {

    private ListView<File> pdfFilesListView = new ListView<>();
    private ObservableList<File> pdfItemsObservableList = FXCollections.observableArrayList();

    public PDFList() {
        pdfFilesListView.setItems(pdfItemsObservableList);
        pdfFilesListView.setOnMouseReleased(mouseEvent -> {
            handleDoubleMouseClickOnPdfItem(mouseEvent);
        });

        pdfFilesListView.setCellFactory(pdfFilesListView -> new PDFCell(new PDFCellCallback() {
            @Override
            public void onCellRemove(int cellIndex) {
                pdfItemsObservableList.remove(cellIndex);
            }
        }));

        pdfFilesListView.setOnDragDropped(dragEvent -> {
            Dragboard dragboard = dragEvent.getDragboard();
            List<File> pdfFiles = dragboard.getFiles().stream().filter(f -> f.getAbsolutePath().endsWith(Constants.PDF_FILE_FORMAT)).collect(Collectors.toList());

            boolean droppedCompleted = false;

            if(pdfFiles != null && !pdfFiles.isEmpty()) {
                pdfItemsObservableList.addAll(pdfFiles);
                droppedCompleted = true;
            }

            dragEvent.setDropCompleted(droppedCompleted);
            dragEvent.consume();

        });

        pdfFilesListView.setOnDragOver(dragEvent -> {
            if(dragEvent.getDragboard().hasFiles()) {
                dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            dragEvent.consume();
        });

    }

    public void add(File item) {
        pdfItemsObservableList.add(item);
    }

    public void addAll(File[] items) {
        pdfItemsObservableList.addAll(items);
    }

    public void clear() {
        pdfItemsObservableList.clear();
    }

    public ObservableList<File> getData() {
        return pdfItemsObservableList;
    }

    public ListView<File> getListView() {
        return pdfFilesListView;
    }

    private void handleDoubleMouseClickOnPdfItem(MouseEvent mouseEvent) {
        if(pdfItemsObservableList.size() == 0) {
            // fixes NullPointer on double clicking empty list
            return;
        }

        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            if(mouseEvent.getClickCount() == Constants.MOUSE_DOUBLE_CLICK_COUNT){
                File selectedItem = pdfFilesListView.getSelectionModel().getSelectedItem();
                PDFHelper.openPDFExternal(selectedItem);
            }
        }
    }
}
