package de.cbl.home.jmerge.list;

import de.cbl.home.jmerge.list.callbacks.PDFCellCallback;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import org.w3c.dom.ls.LSOutput;

import java.io.File;

public class PDFCell extends ListCell<File> {
    public static final int PAGE_INDEX_OFFSET = 1;
    HBox cellContainer = new HBox();
    Label fileNameLabel = new Label("");
    Pane pane = new Pane();
    Button removeCellBtn = new Button("Remove");
    File lastItem;

    public PDFCell(PDFCellCallback callback) {
        super();
        HBox.setHgrow(pane, Priority.ALWAYS);
        removeCellBtn.setOnAction(actionEvent -> {
            callback.onCellRemove(this.getIndex());
        });
        cellContainer.getChildren().addAll(fileNameLabel, pane, removeCellBtn);

        setOnDragDetected(mouseEvent -> {
            System.out.println("on drag detected on item: " + this.getIndex());
            Dragboard dragBoard = this.startDragAndDrop(TransferMode.MOVE);
        });

        setOnDragEntered(mouseEvent -> {
            System.out.println("on drag entered on item: " + this.getIndex());
        });

        setOnDragExited(dragEvent -> {
            System.out.println("on drag exited on item: " + this.getIndex());
        });

        setOnDragOver(dragEvent -> {
            System.out.println("on drag over on item: " + this.getIndex());
        });

        setOnDragDone(dragEvent -> {
            System.out.println("on drag done on item: " + this.getIndex());
        });

        setOnDragDropped(dragEvent -> {
            System.out.println("on drag dropped on item: " + this.getIndex());
        });

    }

    @Override
    protected void updateItem(File item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if(empty || item == null) {
            lastItem = null;
            setGraphic(null);
        }else {
            lastItem = item;
            fileNameLabel.setText(getItemText(item));
            setGraphic(cellContainer);
        }
    }

    private String getItemText(File item) {
        int pageIndex = this.getIndex() + PAGE_INDEX_OFFSET;
        return "Page (" + pageIndex + ") | " + item.getAbsolutePath();
    }
}
