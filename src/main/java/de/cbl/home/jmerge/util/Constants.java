package de.cbl.home.jmerge.util;

public class Constants {
    public static final String PDF_FILE_FORMAT = ".pdf";
    public static final String PDF_FILE_FORMAT_FILTER = "*" + PDF_FILE_FORMAT;
    public static final int MOUSE_DOUBLE_CLICK_COUNT = 2;
    public static final String PDF_FILE_DESCRIPTION = "PDF (*.pdf)";
}
